package org.example.demo;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {
    @Before("execution(* org.example.demo.ShoppingCart.checkout(..))")
    public void beforeLogger(JoinPoint jp) {
        System.out.println(jp.getSignature());

        String arg = jp.getArgs()[0].toString();

        System.out.println("Before Loggers with Argument: " + arg);
    }

    @After("execution(* *.checkout(..))")
    public void afterLogger() {
        System.out.println("After Loggers");
    }

    @Pointcut("execution(* org.example.demo.ShoppingCart.quantity())")
    public void afterReturningPointCut() {
        System.out.println("After Returning PointCut");
    }

    @AfterReturning(pointcut = "afterReturningPointCut()", returning = "retVal")
    public void afterReturning(JoinPoint jp, String retVal) {
        System.out.println("After Returning Signature: " + jp.getSignature());
        System.out.println("After Returning Value: " + retVal);
    }
}
