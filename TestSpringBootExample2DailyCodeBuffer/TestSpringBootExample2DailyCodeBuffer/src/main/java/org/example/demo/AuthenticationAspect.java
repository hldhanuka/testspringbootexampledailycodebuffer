package org.example.demo;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AuthenticationAspect {
    @Pointcut("within(org.example.demo.*)")
    public void authenticatingPointCut(){
        System.out.println("Authentication Point Cut");
    }

    @Pointcut("within(org.example.demo.ShoppingCart)")
    public void authorizationPointCut(){
        System.out.println("Authorization Point Cut");
    }

    @Before("authenticatingPointCut() && authorizationPointCut()")
    public void authenticate(){
        System.out.println("Authenticating the request");
    }
}
