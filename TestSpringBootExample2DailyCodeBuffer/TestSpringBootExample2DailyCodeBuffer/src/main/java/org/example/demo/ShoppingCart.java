package org.example.demo;

import org.springframework.stereotype.Component;

@Component
public class ShoppingCart {
    public void checkout(String status) {
        System.out.println("checkout Method from shoppingCart Called");
    }

    public int quantity() {
        return 2;
    }
}
