package org.example;

import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Scope(scopeName = "prototype")
public class Doctor implements Staff, BeanNameAware {
    private String qulification;

    public String getQulification() {
        return qulification;
    }

    public void setQulification(String qulification) {
        this.qulification = qulification;
    }

    public void assist() {
        System.out.println("Doctor is assisting");
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "qulification='" + qulification + '\'' +
                '}';
    }

    @Override
    public void setBeanName(String s) {
        System.out.println("Set bean name method is called");
    }

    @PostConstruct
    public void postConstruct() {
        System.out.println("Post Construct method is called");
    }


}
